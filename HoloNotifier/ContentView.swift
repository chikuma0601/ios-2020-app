//
//  ContentView.swift
//  HoloNotifier
//
//  Created by chikuma on 2020/6/13.
//  Copyright © 2020 Chikuma. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
